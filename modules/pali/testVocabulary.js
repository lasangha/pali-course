/*
// This will be used when I can get images for the tests
Pali.vocabulary = {
// The list of images to use
imgsList: [],
theWord: "",
theWordAnswer: "",
theWordPos: 0,
// A words list
words: {Amacca:['Minister','minister'],
Assa:['Horse','horse'],
Āvāṭa:['Pit','pit'],
Bhatta:['Rice','rice'],
Bhūpāla:['King','king'],
Brāhmaṇa:['Brahmin','brahmin'],
Cora:['Thief','thief'],
},
// Selects images for the test
selectImgs: function(){

var keys = Object.keys(this.words);
var total = keys.length;
console.log("Selecting the images" + total + " id: " + this.theWordPos);

// Add the correct answer 
this.imgsList.push(this.words[keys[this.theWordPos]][1]);

// I will select images until I have 4
while(this.imgsList.length < 4){
// Get a new number
var newNumber = Math.floor((Math.random() * total) + 1) - 1;
console.log("Selecting a new number, trying with: " + newNumber);
// I will only count it if it is not in the list and it is not equals to the correct answer
if(this.imgsList.indexOf(this.words[keys[newNumber]][1]) == -1){
console.log("Found one" + this.imgsList.indexOf(this.words[keys[newNumber]][1]) + " >> " + this.words[keys[newNumber]][1]);
this.imgsList.push(this.words[keys[newNumber]][1]);
}
}
return this;
},
selectWord: function(){

console.log("Selecting the word");

// Get the keys
var keys = Object.keys(this.words);

// Select a possition, this will be random
this.theWordPos    = Math.floor((Math.random() * keys.length) + 1) - 1;
this.theWord       = keys[this.theWordPos];
this.theWordAnswer = this.words[keys[this.theWordPos]][0];
console.log("------------------" + this.theWordAnswer);

console.log("Selected word: " + this.theWord + " in pos: " + this.theWordPos);

return this;

},
testMeOut: function(){
console.log("Testing a word");
// Get the numbers
$("#testVocabularyWord").html(this.theWord);
$("#testVocabularyImgOption_1").attr('src', "testsImgs/" + this.imgsList[1] + ".jpg");
$("#testVocabularyImgOption_0").attr('src', "testsImgs/" + this.imgsList[0] + ".jpg");
$("#testVocabularyImgOption_3").attr('src', "testsImgs/" + this.imgsList[3] + ".jpg");
$("#testVocabularyImgOption_2").attr('src', "testsImgs/" + this.imgsList[2] + ".jpg");

return this;
},
testAnswer: function(id){
console.log("Testing answer");
$("#testVocabularyWordAnswer").html(this.theWordAnswer);
return this;
}
};
*/

// Shuffle an array
// http://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array-in-javascript
function Extra_shuffle(o){
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}

Pali.vocabulary = {
    // The list of images to use
    options: [],
    theWord: "",
    theWordAnswer: "",
    theWordPos: 0,
    // A words list
    words: {
        Amacca:'Ministro',
        Assa:'Caballo',
        Āvāṭa:'Hueco',
        Bhatta:'Arroz',
        Bhūpāla:'Rey',
        Brāhmaṇa:'Brahmin',
        Buddha:'El Buda',
        Tathāgata:'El Buda',
        Sugata :'El Buda',
        Canda:'Luna',
        Cora:'Ladrón',
        Dāraka:'Niño',
        Dhamma:'Verdad, La Doctrina',
        Dhīvara:'Pescador',
        Dīpa:'Lampara, Isla',
        Gāma:'Villa',
        Hattha:'Mano',
        Kakaca:'Serrucho',
        Kassaka:'Granjero',
        Khagga:'Espada',
        Kukkura:'Perro',
        Sunakha:'Perro',
        Soṇa:'Perro',
        Kumāra:'Niño',
        Maccha:'Pez',
        Magga:'Camino',
        Manussa:'Humano, Hombre',
        Mātula:'Tío',
        Miga:'Venado',
        Nara:'Hombre, Persona',
        Purisa:'Hombre, Persona',
        Odana:'Arroz Cocido',
        Pabbata:'Montaña',
        Pāda:'Pie',
        Paṇḍita:'Hombre sabio',
        Pañha:'Pregunta',
        Pāsāda:'Palacio',
        Pāsāṇa:'Piedra',
        Patta:'Tazón',
        Piṭaka:'Canasta',
        Putta:'Hijo',
        Rajaka:'Lavador',
        Ratha:'Vehículo, carroza',
        Rukkha:'Árbol',
        Sagga:'Cielo',
        Sahāya:'Amigo',
        Sahāyaka: 'Amigo',
        Mitta :'Amigo',
        Sakaṭa:'Carreta',
        Samaṇa:'Monje, Recluso',
        Sappa:'Serpiente',
        Sara:'Flecha',
        Sāṭaka:'Adorno',
        Sāvaka:'Disípulo',
        Sigāla:'Chacal',
        Sopāna:'Escalera',
        Suka:'Perico',
        Suva:'Perico',
        Sūkara:'Cerdo',
        Varāha:'Cerdo',
        Suriya:'Sol',
        Upāsaka:'Devoto no laico',
        Vāṇija:'Mercader',
        Vihāra:'Monasterio',
        Yācaka:'Mendigo',
        Āgacchati:'Venir',
        Aharati:'Traer',
        Āruhati:'Subir',
        Bhāsati:'Hablar',
        Bhuñjati:'Comer',
        Chindati:'Cortar',
        Ḍasati:'Morder',
        Dhāvati:'Corre',
        Dhovati:'Lavar',
        Gacchati:'Ir',
        Hanati:'Matar',
        Harati:'Llevar',
        Hruhati:'Descender',
        Icchati:'Desea',
        Kasati:'Arar',
        Khādati:'Comer',
        Khaṇati:'Cava',
        Nikkhamati:'Irse',
        Otarati:'Descender',
        Pacati:'Cocinar',
        Paharati:'Golpear',
        Pakkosati:'Llamar',
        Passati:'Ver',
        Patati:'Caer',
        Pucchati:'Preguntar',
        Rakkhati:'Proteger',
        Sayati:'Dormir',
        Vandati:'Saludar',
        Vijjhati:'Disparar',
        Yācati:'Implorar'
    },
    selectWord: function(){

        console.log("Selecting the word");

        // Get the keys
        var keys = Object.keys(this.words);
        var total = keys.length;

        // Select a possition, this will be random
        this.theWordPos    = Math.floor((Math.random() * keys.length) + 1) - 1;
        this.theWord       = keys[this.theWordPos];
        this.theWordAnswer = this.words[keys[this.theWordPos]];
        console.log("Selected word: " + this.theWord + " in pos: " + this.theWordPos + " answer: " + this.theWordAnswer);

        // Create options and add the correct answer
        this.options.push(this.theWordAnswer);

        // I will select images until I have 4
        while(this.options.length < 4){
            // Get a new number
            var newNumber = Math.floor((Math.random() * total) + 1) - 1;
            console.log("Selecting a new number, trying with: " + newNumber);
            // I will only count it if it is not in the list and it is not equals to the correct answer
            if(this.options.indexOf(this.words[keys[newNumber]]) == -1){
                console.log("Found one" + this.options.indexOf(this.words[keys[newNumber]]) + " >> " + this.words[keys[newNumber]]);
                this.options.push(this.words[keys[newNumber]]);
            }
        }

        // Shuffle the options
        Extra_shuffle(this.options);
        return this;

    },
    testMeOut: function(){
        console.log("Testing a word");
        // Get the numbers
        $("#testVocabularyWord").html(this.theWord);
        $("#testVocabularyOption_0").html(this.options[0]);
        $("#testVocabularyOption_1").html(this.options[1]);
        $("#testVocabularyOption_2").html(this.options[2]);
        $("#testVocabularyOption_3").html(this.options[3]);

        return this;
    },
    testAnswer: function(id){
        console.log("Testing answer");
        $("#testVocabularyWordAnswer").html(this.theWordAnswer);
        return this;
    }
};

