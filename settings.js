/*******************************************************************************
 *
 * Cala
 *
 */

Cala.frontPage = "pali/default";

// What device is this? Android|iOs|Comp
// Set the correct option for internal storage and other minor aspects
Cala.Device = 'comp';

/*******************************************************************************
 *
 * jDrupal
 *
 */
// Set the site path (without the trailing slash). 
Drupal.settings.site_path = false;

// Set the Service Resource endpoint path.
//Drupal.settings.endpoint = "rest";
