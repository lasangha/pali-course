/****************************************************************************
/*!
 * Cala Framework: To make your life simpler
 * @requires jQuery v1.5? or later, jDrupal (maybe other things too)
 *
 * Copyright (c) 2015 Twisted Head
 * License: MIT

 * Include this AT THE BOTTOM of your pages, that is all you need to do.
 * Cala_detectBrowser.isThisAnApp().loadScripts();
 *           | |      
 *   ___ __ _| | __ _ 
 *  / __/ _` | |/ _` |
 * | (_| (_| | | (_| |
 *  \___\__,_|_|\__,_|

 *****************************************************************************/                   
// Version
var version = "0.3.1-mod";

//! The main instance of Cala
var Cala = Cala || {};

//! Internal useful things
VAR_GO_NOT_LOGGED_IN = '?x=login';

//! Some variables
// By default I think I run on a computer, this will change automatically
Cala.onApp = false;

// Some other constants
// Parameters sent via url
var params = false;

/**
 * Run stuff during boot up, if you want me to run stuff, let me know
 */
var Cala_runMe = [];

/*****************************************************************************/
//
// Boot stuff
//
//////////////////////////////////////////////////////////////////////////////

/**
 * This is where it all begins, this should be run at the very beggining 
 * of the page load, or when document is ready, but you never call this directly,
 * you need to call whereAmI();
 */
function Cala_initMe(){

    Cala_say("Booting up the car!");

    // I will only try to connect if there is a server set, else, this is a non central server system
    if(Drupal.settings.site_path === false){
        console.log("I run local, no central server");
        Cala_justRunThisHelper(); 
        return true;
    }
    // Perform a System Connect call.
    // @todo This should be core for the system to run
    system_connect({
        success:function(result){
            if (Drupal.user.uid === 0) {
                message = "Hello World, Visitor!";
            }else {
                message = "Hello World, " + Drupal.user.name + "!";
            }

            Cala_say("Hello: " + message);
 
            Cala_justRunThisHelper();
        },
        error:function(xhr, status, message) {
            console.log("Error on system call, ther could be a problem with the internet or the main server");
        }
    });

}

// Helper function to run the bootup functions
// Do not call directly, you should not need to
function Cala_justRunThisHelper(){

    // Get the params
    params = Cala_getUrlVars();

    // Run things that people want me to run
    for(i = 0; i < Cala_runMe.length; i++){
        Cala_say("running..." + i);
        Cala_justRunThis(Cala_runMe[i]);
    }
}

// Actually run functions
function Cala_justRunThis(what){
    what();
}

/**
 * This is what you should call from your pages, call it at the bottom
 */
function Cala_boot(){

    Cala_say("I am on a computer");
    Cala_runMe.push(Cala_loadThisPath);
    Cala_runMe.push(Cala_menuParse);
    $(document).ready(Cala_initMe);

}

//! Sets code to be run when the page is ready
function Cala_runOnReady(runMe){
    $(document).ready(runMe);
}

// 'Fix' the top menu with additional links and put the users name on it
function Cala_menuParse(){

    Cala_say("Am I logged in? Should I change the menu?");

    if (Drupal.user.uid > 0) {

        $("#logMeIn").text("");

        $("#topMenuButton").html(Drupal.user.name);

        newLink = '' +
            '<li class="divider"></li>' +
            '<a href="?x=myAccount" class="list-group-item">' +
            '<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>' +
            ' Configuración</a>' +
            '<a href="#" onClick="return Cala_logMeOut();" class="list-group-item">' +
            '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span>' +
            ' Salir</a>';

        $("#userMenu").append(newLink);

    }else{
        Cala_say("No, not logged in at all");
    }

}

/***************************************************************************
 *
 * Local storage
 *
 */

//! Store keys in local storage
function keyStore(key, value){
    Cala_say("Storing key: " + key);
    window.localStorage.setItem(key, value);
    return true;
}

//! Get key from local storage
function keyGet(key, defaultValue){
    var value = window.localStorage.getItem(key);
    if(value === null){
        Cala_say("No value found, I will use the default");
        value = defaultValue;
    }
    Cala_say("Gotten Key: " + key + " with value: " + value);
    return value;
}

//! Remove key from local storage
function removeKey(theKey){

    Cala_say("Removing key: " + theKey);
    // Remove them all
    if(theKey === ''){

    }else{
        window.localStorage.removeItem(theKey);
    }
}

/**
 * Remove keys after logout
 */
function _logMeOutRmKeys(){

    keysGone = [VAR_CURRENT_USER_NAME, VAR_CURRENT_SESSION_KEY];

    // If I don't do it like this, it won't be able to remove them all
    for(i = 0; i < keysGone.length; i++){
        removeKey(keysGone[i]);
    }

    iGoTo('index.html');

}

/*****************************************************************************/
//
// Messaging and alerts
//
//////////////////////////////////////////////////////////////////////////////
// Use any of the standard bootstrap options + t if you want to limit the message for some time
Cala.Messages = {

    //! Clear the alert messages
    clear: function(){
        $("#Cala_alertMessages").hide('slow', function(){
            $("#Cala_alertMessages").html('');
        });
        return this;
    },

    /**
     * Helper function to actually present messages to the user, you should never
     * call this directly
     */
    _alert: function(what, _type, t){

        console.log("Internal message" + t);

        var options = {text:what, type:_type};

        if(t === undefined){
            options.permanent = true;
        }else{
            options.time = t;
        }

        $.smkAlert(options);

        return this;
    },

    //! Alert messages to the user
    danger: function(what, t){
        this._alert(what, 'danger', t);
        return this;
    },

    // Alert warning messages to the user
    warning: function(what, t){
        this._alert(what, 'warning', t);
        return this;
    },

    // Alert information messages to the user
    info: function(what, t){
        this._alert(what, 'info', t);
        return this;
    },

    //! Success messages
    success: function(what, t){
        this._alert(what, 'success', t);
        return this;
    }
};

// Set a 'working' indicator
Cala.workingOn = {

    start: function(container){

        console.log("Working on...ini");

        $.smkProgressBar({
            element: container,
            status: 'start'
        });
    },
    end: function(container){

        console.log("Working on...end");
        $.smkProgressBar({
            element: container,
            status: 'end'
        });
    }

};

/*****************************************************************************/
//
// Other tools
//
//////////////////////////////////////////////////////////////////////////////

//! Add a css
function Tpl_addCss(which){

    // Is this an external css
    if(strpos(which, 'http', 0) === false){
        Cala_say("Adding an internal css file");
        which = "tpl/" + which;
    }

    Cala_say("Adding a css" + which);
    $('head').append( $('<link rel="stylesheet" type="text/css" />').attr('href', which) );
}

/**
 *  I parse a tpl and replace it with some values
 *  @param tpl The template
 *  @param values An object with the values to replace
 *  @param bl Do you want me to replace the breaklines (\n) with <br />
 */
function parseTpl(tpl, values, bl){
    // Loop each value
    for (var key in values) {
        var theKey = "{" + key + "}";
        var re = new RegExp(theKey, "g");
        tpl = tpl.replace(re, values[key]);
    }
    // Should I add html breakLines?
    if(bl === true){
        tpl = textAddBreakLines(tpl);
    }
    return tpl;
}

// Parse a date from a UTC timestamp
// Read more https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Text_formatting
function dateParse(timestamp){

    //var utcSeconds = 1234567890;
    var d = new Date(0);
    d.setUTCSeconds(timestamp);
    timestamp = d.toString();

    return timestamp;

}

/**
 * I will redirect somewhere
 * @deprecated use Cala.iGoTo(goTo);
 */
function iGoTo(goTo){
    Cala_say("Going to: " + goTo);
    window.location.href = goTo;
}

/**
 * String poss of a word/string in a string
 * http://phpjs.org/functions/strpos/
 * @param haystack where are you looking in?
 * @param needle what are you looking for?
 * @param offset where do you want to start the search?
 */
function strpos(haystack, needle, offset) {
    //  discuss at: http://phpjs.org/functions/strpos/
    // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // improved by: Onno Marsman
    // improved by: Brett Zamir (http://brett-zamir.me)
    // bugfixed by: Daniel Esteban
    //   example 1: strpos('Kevin van Zonneveld', 'e', 5);
    //   returns 1: 14

    var i = (haystack + '')
        .indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
}

/**
 * Parse url parameters
 * http://jquery-howto.blogspot.com/2009/09/get-url-parameters-values-with-jquery.html
 */
function Cala_getUrlVars(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
        Cala_say("Found param: " + hash[0] + hash[1]);
    }
    return vars;
}

// Get a param coming from the url or a default value
function Cala_paramsGet(which, def){
    // TMP solution, I will move all params in here if I can
    params = window.params;
    if(params[which] !== undefined && params[which] !== ''){
        Cala_say("Found param with value" + params[which]);
        return params[which];
    }
    else{
        Cala_say("No custom path");
        return def;
    }	
}

// Add html break lines to the text
function textAddBreakLines(text){
    var theKey = "\n";
    var re = new RegExp(theKey, "g");
    text = text.replace(re, "<br />");
    return text;
}

//! Get the correct page for this path
function Cala_loadThisPath(){
    Cala.workingOn.start("body");
    Cala_say("Loading the current path: " + params.x);
    path = Cala_paramsGet('x', Cala.frontPage);
    $("#mainContent").load("modules/" + path + ".html?ppp=" + Math.floor(Math.random() * 1000),
            function(){Cala.workingOn.end("body");});
}

//! Say something
function Cala_say(what){
    console.log(what);
}

/**
 * Check to see if the user is logged in, this will be extended to use all the
 * Drupal perms
 * It does not really work at the moment
 * @param redirect If you want to send the person somewhere, let me know
 */ 
function users_loggedInRequired(redirect){
    if(keyGet(VAR_CURRENT_USER_NAME, '') === ''){
        if(redirect !== ''){
            iGoTo(redirect);
        }else{
            return false;
        }
    }
    return true;
}

/**
 * Opens external links in browser or in an external brower if running on phonegap
 */
function Cala_externalLink(linkId){

    // I will only act if I am running on an app
    if(Cala.onApp === false){
        return true;
    }else{
        // Get the path
        var whereTo = $("#" + linkId).attr("href");
        if(whereTo.indexOf("http") === 0){
            console.log("External link");
        }else{
            whereTo = "http://meditacion.lasangha.org/" + whereTo;
        }
        console.log("Clicked me: " + whereTo);
        navigator.app.loadUrl(whereTo, { openExternal:true });
        return false; 
    }
}

/*****************************************************************************/
//
// User Management
//
//////////////////////////////////////////////////////////////////////////////
/**
 * Log a user in
 */
function Cala_logMeIn(){

    userName = $("#myUserName").val();
    password = $("#myPwd").val();

    if(userName === "" || password  === ""){
        console.log("Something is missing");
        Cala.Messages.clear().warning("Faltan algunos campos");
        return false;
    }

    _Cala_logMeIn(userName, password);

    return false;
}

/**
 * Helper function to actually log someone in
 */
function _Cala_logMeIn(userName, password){

    Cala.workingOn.start("body");

    user_login(userName, password, {
        success:function(result){
            alert('Hola ' + result.user.name + '!');
            iGoTo("index.html");
        },
        error:function(xhr, status, message){
            Cala.Messages.clear().warning("Datos incorrectos, ¿Quizá olvidó su clave?");
            console.log("Unable to login");
        }
    });

    Cala.workingOn.end("body");
}

/**
 * Set details when visiting the account
 */
function Cala_usersSetEditAccount(){

    console.log("Visiting my account: " + Drupal.user.uid);

    user_load(Drupal.user.uid, {
        success:function(account) {
            $("#myUserName").val(account.name);
            $("#myEmail").val(account.mail);
            console.log("Loaded user " + account.name);
        }
    });
}

/**
 * Update account details
 */
function Cala_usersUpdateAccount(){

    Cala.workingOn.start("body");

    console.log("Updating account");

    var account = {
        uid: Drupal.user.uid,
        name: $("#myUserName").val(),
        mail: $("#myEmail").val(),
        current_pass: $("#myAccountCurrentPwd").val()
    };

    if($("#myAccountPwd").val() !== ""){
        console.log("Using a new password: " + $("#myAccountPwd").val());
        account.pass = $("#myAccountPwd").val();
    }

    user_update(account, {
        success:function(result) {
            console.log('Updated user #' + result.uid);
            Cala.workingOn.end("body");

            alert("Listo!");
            iGoTo("?x=myAccount");
        },
        error:function(result){
            Cala.Messages.clear().warning(Cala_parseErrorMessagesDrupal(result));
            Cala.workingOn.end("body");
        }
    });
}

/**
 * Parse error messages sent by drupal
 */
function Cala_parseErrorMessagesDrupal(result){

    // Get the response text
    errorMessages = JSON.parse(result.responseText);

    // The full error message(s)
    fullErrorMessage = "";

    if(errorMessages.form_errors !== undefined){

        console.log("Getting form_errors");

        // Get all error messages
        formErrors = errorMessages.form_errors;

        formErrorsKeys = Object.keys(formErrors);

        // Iterate and store them in a single error message
        for (var i = 0; i < formErrorsKeys.length; i++) {
            console.log("Adding one error message: " + i);
            fullErrorMessage = fullErrorMessage + formErrors[formErrorsKeys[i]];
        }
    }else{
        fullErrorMessage = errorMessages;
    }

    return fullErrorMessage;

}

/**
 * Log a user in
 */
function Cala_userRegister(){

    userEmail = $("#myEmailR").val();
    password = $("#myPwdR").val();

    if(userEmail === "" || password  === ""){
        console.log("Something is missing");
        Cala.Messages.clear().warning("Faltan algunos campos");
        return false;
    }

    var account = {
        name: userEmail,
        mail: userEmail,
        pass: password
    };

    user_register(account, {
        success:function(result) {
            console.log('Registered user #' + result.uid);
            _Cala_logMeIn(userEmail, password);
        },
        error: function(result){
            Cala.Messages.clear().warning("Hubo un error, quizá ya este correo existe, si este es su correo y olvidó la clave, puede solicitar una nueva.");
        }
    });

    return false;

}

/**
 * Log a user out
 */
function Cala_logMeOut(){

    Cala.workingOn.start("body");
    user_logout({
        success:function(result){
            if (result[0]) {
                alert("Hasta luego!");
                iGoTo("index.html");
            }
        }
    });
    Cala.workingOn.end("body");
}

/**
 * Request a new password
 */
function Cala_usersPasswordReset(){

    userName = $("#myRecoverEmail").val();

    if(userName === ""){
        console.log("Something is missing");
        Cala.Messages.clear().warning("Requiero al menos un usuario para este procedimiento");
        return false;
    }

    user_request_new_password(userName, {
        success: function(result) {
            if (result[0]) {
                console.log("All good, a new password in underway");
                Cala.Messages.clear().success("Las instrucciones para terminar con el proceso serán enviadas a su correo electrónico");
            }
        },
        error: function(){
            Cala.Messages.clear().danger("Hubo un error, quizá el correo no existe. Favor intente de nuevo.");
        }
    });
    return false;
}

/**
 * Creates a path for the audios depending if it is a web version or an app
 */
function Cala_createAudioPath(){
    if(Cala.onApp === true){
        //return 'file:///';
        return '';
    }else{
        return '';
    }
}

// Loads the extra scripts for the app
// Not yet used
function Cala_loadAppScripts(){
    console.log("Loading app scripts");
    $.getScript("cordova.js", function( data, textStatus, jqxhr ) {
        $.getScript("cordova.index.js", function(){console.log("Loaded cordova.index.js");});
        console.log("Loaded cordova.js");
    });
}

// Hide/Display the top menu
function Cala_displayTopMenu(){
    Cala_say("Showing/hidding top menu");
    $('#Cala_topMenu').toggle('slow');
    window.scrollTo(0,0);
    return false;
}
$('#Cala_topMenu').toggle(); // This should happen on each page load

// Browser detection, this is only to automatically know if this is a browser or an app
// @todo detect iOs|android|MS
var Cala_detectBrowser = {
    getProtocol: function(){
        protocol = document.location.protocol;
        console.log("Current protocol is: " + protocol);
        return this;
    },
    isThisAnApp: function(){
        console.log("Detecting protocol");
        if(document.location.protocol === 'file:'){
            console.log("Running on an app");
            Cala.onApp = true;
        }else{
            console.log("Looks like a normal browser to me");
        }
        return this;
    },
    loadScripts: function(){
        if(Cala.onApp === false){
            Cala_boot();
        }else{
            Cala_loadAppScripts();
        }
    }
};


