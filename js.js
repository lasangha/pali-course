// Add html break lines to the text
function textAddBreakLines(text){
    var theKey = "\n";
    var re = new RegExp(theKey, "g");
    text = text.replace(re, "<br />");
    return text;
}

// From cala
function parseTpl(tpl, values, bl){
    // Loop each value
    for (var key in values) {
        var theKey = "{" + key + "}";
        var re = new RegExp(theKey, "g");
        tpl = tpl.replace(re, values[key]);
    }
    // Should I add html breakLines?
    if(bl === true){
        tpl = textAddBreakLines(tpl);
    }
    return tpl;
}

// The main pali object
var Pali = {

    // The theory for the lessons
    theory:
    {l1:{e:'Nominative case "The" (The person or object that does the action)',
            s:{t: '-o', e: 'nara + o = naro → the man'},
            p:{t: '-ā', e:'nara + ā = narā → the men'}
        },
    l4:{e:'ablative case “From”',
        s:{t: '-ā / -mhā / -smā', e: 'nara + ā/mhā/smā = narā / naramhā / narasmā → From the man'},
        p:{t: 'ehi; -ebhi (archaic)', e:'nara + ehi = narehi / narebhi → From the men'}
    }},
    examples: {
        l1:{
            s:['Naro bhāsati: The man speaks','Mātulo pacati: The uncle cooks','Kassako kasati: The farmer ploughs'],
            p:['Narā bhāsanti: Men speak','Mātulā pacanti: Uncles cook','Kassakā kasanti: Farmers plough'],
        }
    },
    // Translation tests for each lesson
    tests: {
        l1: [
        {q: 'Tathāgato dhammaŋ bhāsati', r: 'The Buda speaks the dhamma (truth)'},
        {q: 'Brāhmaṇā odanaŋ bhuñjanti', r: 'The Brahmins eats cooked rice'}
        ]
    },

    // Generate the questions for translations
    generateTrans: function(lines){
        console.log("Generating questions");
        var tpl = "<li><div id='q_{id}' class='pali_transOrig' onClick='Pali.displayAnswer(\"r_{id}\")'>{q}</div><div id='r_{id}'>{r}</div></li>";
        // I know, I am old school :)
        var i = 0;
        for(var trans in lines){
            //trans.id = i;
            lines[trans].id = i;
            $('#testMeOutUL').append(parseTpl(tpl, lines[trans], true));
            $("#r_"+i).toggle();
            i++;
        }
    },
    displayAnswer: function(id){
        $("#"+id).toggle();
    },
    displayTheory: function(lesson, id){
        console.log("Displaying lesson");
        //$("#theoryTestMeOut").attr('onClick', 'Pali.generateTrans(Pali.tests.'+id+')');
        $("#theoryExplanation").html(lesson.e);
        $("#theorySingularTerminations").html(lesson.s.t);
        $("#theorySingularExample").html(lesson.s.e);
        $("#theoryPluralTerminations").html(lesson.p.t);
        $("#theoryPluralExample").html(lesson.p.e);
    }
};
Pali.generateTrans(Pali.tests.l1);
Pali.displayTheory(Pali.theory.l1, 'l1');
$('#testMeOut').toggle();
